package isci

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEnvironmentAnyValue(t *testing.T) {
	assert := assert.New(t)
	ev := "GO_ISCI_TEST_ENVANYVALUE"

	env := EnvironmentAnyValue{Name: ev}
	assert.Implements((*environmentCheck)(nil), env)

	os.Unsetenv(ev)
	assert.False(env.Check(), "When the env var is not set, Check() should return false")

	os.Setenv(ev, "")
	assert.True(env.Check(), "When the env var is set, but is empty string, Check() should return true")

	os.Setenv(ev, "foo")
	assert.True(env.Check(), "When the env var is set to any value, Check() should return true")
}

func TestEnvironmentWithValue(t *testing.T) {
	assert := assert.New(t)
	ev := "GO_ISCI_TEST_ENVWITHVALUE"

	env := EnvironmentWithValue{Name: ev, Value: "is_test"}
	assert.Implements((*environmentCheck)(nil), env)

	os.Unsetenv(ev)
	assert.False(env.Check(), "When the env var is not set, Check() should return false")

	os.Setenv(ev, "foo")
	assert.False(env.Check(), "When the env var is set, but is not the correct value, Check() should return false")

	os.Setenv(ev, "is_test")
	assert.True(env.Check(), "When the env var is set to the correct value, Check() should return true")
}
