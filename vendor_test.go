package isci_test

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	. "gitlab.com/tybrown/go-isci"
)

func TestVendor_AnyValue(t *testing.T) {
	assert := assert.New(t)
	ev1 := "GO_ISCI_TEST_VENDOR_ANYVALUE1"
	ev2 := "GO_ISCI_TEST_VENDOR_ANYVALUE2"

	vendor := NewVendor("isci Test", EnvironmentAnyValue{Name: ev1}, EnvironmentAnyValue{Name: ev2})

	os.Unsetenv(ev1)
	os.Unsetenv(ev2)
	assert.False(vendor.Check(), "When both env vars are not set, Check() should return false")

	os.Setenv(ev1, "")
	assert.False(vendor.Check(), "When only one env var is set, but is empty string, Check() should return false")

	os.Setenv(ev1, "foo")
	assert.False(vendor.Check(), "When only one env var is set to any value, Check() should return false")

	os.Setenv(ev1, "")
	os.Setenv(ev2, "")
	assert.True(vendor.Check(), "When both env vars are set, but both empty string, Check() should return true")

	os.Setenv(ev1, "foo")
	os.Setenv(ev2, "bar")
	assert.True(vendor.Check(), "When both env vars are set to any value, Check() should return true")
}

func TestVendor_WithValue(t *testing.T) {
	assert := assert.New(t)
	ev1 := "GO_ISCI_TEST_VENDOR_WITHVALUE1"
	ev2 := "GO_ISCI_TEST_VENDOR_WITHVALUE2"

	vendor := NewVendor("isci Test", EnvironmentWithValue{Name: ev1, Value: "foo"}, EnvironmentWithValue{Name: ev2, Value: "bar"})
	assert.Implements((*fmt.Stringer)(nil), vendor, "Ensure Vendor implmements fmt.Stringer interface")

	os.Unsetenv(ev1)
	os.Unsetenv(ev2)
	assert.False(vendor.Check(), "When both env vars are not set, Check() should return false")

	os.Setenv(ev1, "")
	assert.False(vendor.Check(), "When only one env var is set, but is empty string, Check() should return false")

	os.Setenv(ev1, "foo")
	assert.False(vendor.Check(), "When only one env var is set to correct value, Check() should return false")

	os.Setenv(ev1, "")
	os.Setenv(ev2, "bar")
	assert.False(vendor.Check(), "When only second env var is set to correct value, Check() should return false")

	os.Setenv(ev1, "")
	os.Setenv(ev2, "")
	assert.False(vendor.Check(), "When both env vars are set, but both empty string, Check() should return false")

	os.Setenv(ev1, "foo")
	os.Setenv(ev2, "bar")
	assert.True(vendor.Check(), "When both env vars are set to correct value, Check() should return true")
}
