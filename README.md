# isci

[![GoDoc](https://img.shields.io/badge/pkg.go.dev-doc-blue)](http://pkg.go.dev/gitlab.com/tybrown/go-isci)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/tybrown/go-isci)](https://goreportcard.com/report/gitlab.com/tybrown/go-isci)

Package isci provides details about the current Continuous Integration environment.
This is a partial Go port of [https://github.com/watson/ci-info](https://github.com/watson/ci-info)

## Examples

### IsCI

```golang
isci.IsCI() // returns true if in CI environment
```

### WhichCI

```golang
vendor := isci.WhichCI() // returns a *Vendor if in a CI Environment, otherwise returns nil
if vendor == nil {
    fmt.Println("Not in CI Environment")
} else {
    fmt.Println("In CI Environment:", vendor)
}
```
