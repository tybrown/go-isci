package isci

// Vendor is a CI Vendor object
type Vendor struct {
	Name    string
	EnvVars []environmentCheck
}

// NewVendor creates a new Vendor object
func NewVendor(name string, envs ...environmentCheck) Vendor {
	return Vendor{
		Name:    name,
		EnvVars: envs,
	}
}

// Check will return true/false if the environment matches the expected parameters for the CI Vendor
func (v Vendor) Check() bool {
	vals := make([]bool, len(v.EnvVars))
	for i, env := range v.EnvVars {
		if env.Check() {
			vals[i] = true
		}
	}

	// effective an All() block
	for _, v := range vals {
		if !v {
			return false
		}
	}
	return true
}

// String implements the fmt.Stringer interface
func (v Vendor) String() string {
	return v.Name
}
