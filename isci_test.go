package isci_test

import (
	"fmt"
	"os"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/tybrown/go-isci"
)

var vendorsLock = sync.Mutex{}

func Test(t *testing.T) {
	assert := assert.New(t)
	ev := "GO_ISCI_TEST"

	vendorsLock.Lock()
	defer vendorsLock.Unlock()

	vendor := isci.NewVendor("isci Test", isci.EnvironmentAnyValue{Name: ev})
	isci.Vendors = append(isci.Vendors, vendor)
	defer func() {
		isci.Vendors = isci.Vendors[0 : len(isci.Vendors)-1]
	}()

	os.Unsetenv(ev)
	assert.False(isci.IsCI(), "When the env var is unset, IsCI() should return false")
	assert.Nil(isci.WhichCI(), "When the env var is unset, WhichCI should return nil")

	os.Setenv(ev, "foo")
	assert.True(isci.IsCI(), "When the env var is set, IsCI() should return true")
	assert.Equal(*isci.WhichCI(), vendor, "When the env var is set, WhichCI() should return the correct Vendor object")
}

func Example_isCI() {
	isci.IsCI() // returns true if in CI environment
}

func Example_whichCI() {
	vendor := isci.WhichCI() // returns a *Vendor if in a CI Environment, otherwise returns nil
	if vendor == nil {
		fmt.Println("Not in CI Environment")
	} else {
		fmt.Println("In CI Environment:", vendor)
	}
}
