// Package isci provides details about the current Continuous Integration environment.
// This is a partial Go port of https://github.com/watson/ci-info
package isci

// IsCI detects if currently running in a CI Environment, returns true if in CI, or false if not
func IsCI() bool {
	return WhichCI() != nil
}

// WhichCI detects which CI environment we're currently running in. Returns a *Vendor if running in CI,
// or returns nil if not running in CI.
func WhichCI() *Vendor {
	for _, vendor := range Vendors {
		if vendor.Check() {
			return &vendor
		}
	}
	return nil
}
