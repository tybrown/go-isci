package isci_test

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	. "gitlab.com/tybrown/go-isci"
)

type vendorsJSON []struct {
	Name string `json:"name"`
}

func TestVendors_MatchUpstream(t *testing.T) {
	assert := assert.New(t)

	vendorsLock.Lock()
	defer vendorsLock.Unlock()

	url := "https://raw.githubusercontent.com/watson/ci-info/master/vendors.json"

	resp, err := http.Get(url)
	if err != nil {
		assert.FailNow(err.Error(), "Error when fetching vendors.json from CI repo")
	}

	if resp.Body != nil {
		defer resp.Body.Close()
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		assert.FailNow(err.Error(), "Error when decoding vendors.json body")
	}

	upstreamVendors := vendorsJSON{}
	err = json.Unmarshal(body, &upstreamVendors)
	if err != nil {
		assert.FailNow(err.Error(), "Error unmarshalling vendors.json")
	}

	// Diff the list of vendor names
	var ourVendorNames []string
	var upstreamVendorNames []string

	for _, vendor := range Vendors {
		ourVendorNames = append(ourVendorNames, vendor.Name)
	}

	for _, vendor := range upstreamVendors {
		upstreamVendorNames = append(upstreamVendorNames, vendor.Name)
	}

	assert.ElementsMatch(upstreamVendorNames, ourVendorNames, "We should have the same vendors as upstream vendors.json")
}
