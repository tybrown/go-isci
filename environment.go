package isci

import (
	"os"
)

// environment is an interface that allows for a specific value, or any value
type environmentCheck interface {
	Check() bool
}

// EnvironmentAnyValue only requires a check that the Name exists
type EnvironmentAnyValue struct {
	Name string
}

// Check implements the environmentCheck interface and returns true if the CI environment
// matches the name
func (e EnvironmentAnyValue) Check() bool {
	_, exists := os.LookupEnv(e.Name)
	return exists
}

// EnvironmentWithValue checks if the Name exists, and it's value matches too
type EnvironmentWithValue struct {
	Name  string
	Value string
}

// Check implements the environmentCheck interface and returns true if the CI environment
// matches the name and the value
func (e EnvironmentWithValue) Check() bool {
	env := os.Getenv(e.Name)
	return env == e.Value
}
